<?php

namespace App\Controller;

use App\Entity\Article;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/example", name="get_example")
     */
    public function index(Request $request): Response
    {
        $request->request->all();
        return new Response("hello there");
    }


    /**
     * @Route("/api/articles/{id}" , methods={"GET"}) 
     * 
     */
    public function getArticle(
        SerializerInterface $serializer,
        Article $article
    )
    {
        if (!$article){
            throw new NotFoundHttpException("Shit!!! Article with id  was not found");
        } 

        $data = $serializer->serialize($article, 'json');    
        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');
    
        return $response;
    } 

    /**
     * @Route("/api/articles", name="articles_lists", methods={"GET"})
     */
    public function getArticles(EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $articles = $em->getRepository(Article::class)->findAll();

        $data = $serializer->serialize($articles, 'json');
        
        $response = new Response($data, Response::HTTP_ACCEPTED);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/api/articles", name="article_create", methods={"POST"}) 
     */
    public function postArticle(
        Request $request,
        SerializerInterface $serializer,
        EntityManagerInterface $em
    )
    {
        $data = $request->getContent();
    
        $article = $serializer->deserialize($data, Article::class, 'json');
        
        try{
            $em->persist($article);
            $em->flush();
        } catch (Exception $e){
            throw $e;
        } 

        return new Response('Created', Response::HTTP_CREATED);       
        
    }  
}
